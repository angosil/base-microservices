# Project README

This README provides an overview and instructions for setting up and using the API REST microservice project. The 
project is built using Python Flask and utilizes Docker and Docker Compose to provide a development environment. It 
also includes a Dockerfile to create a Docker image for deployment, along with a Dockerfile.prod optimized for 
production environments. The project is hosted on GitLab and includes a GitLab CI configuration file.

## Setup and Usage

To set up and run the API REST microservice project, please follow these steps:

1. Clone the GitLab repository to your local machine.
2. Make sure you have Docker and Docker Compose installed.
3. Navigate to the project's root directory.

### Development Environment

You have the option to use the `docker-compose` command directly or use the provided scripts for starting a development environment based on Docker Compose.

- Use the `run_docker_compose.sh` script to start a development environment or a production-like environment.
- Use the `build_docker_compose.sh` script for creating an isolated environment to run sequential steps similar to the GitLab CI pipeline.

Please make sure the scripts are executable by running the following command:

```shell
chmod +x scripts/run_docker_compose.sh
chmod +x scripts/build_docker_compose.sh
chmod +x scripts/hard_clean.sh
```

To start the development environment using the ``run_docker_compose.sh`` script, run the appropriate command:

```bash
./scripts/run_docker_compose.sh       # Start development environment
./scripts/run_docker_compose.sh prod  # Start production-like environment
```

The script automatically selects the appropriate Docker Compose configuration file based on the provided parameter 
(prod for production-like environment). If no parameter is provided, the script starts the development environment 
using docker-compose.yaml file.

To create an isolated environment and execute sequential steps using the build_docker_compose.sh script, run the 
following command:

```bash
./scripts/build_docker_compose.sh
```

To perform a hard clean of your Docker environment, use the hard_clean.sh script. This script stops all running 
containers, deletes all containers, images, volumes, and networks. Run the following command:

```bash
./scripts/hard_clean.sh
```

Please refer to the script files in the scripts directory for more details on their functionalities.

## Project Structure

The project follows the following directory structure:

```bash
.
├── app
│   ├── coverage.xml
│   ├── Dockerfile
│   ├── Dockerfile.prod
│   ├── entrypoint.prod.sh
│   ├── entrypoint.sh
│   ├── flake-report
│   ├── htmlcov
│   ├── manage.py
│   ├── src
│   ├── requirements.ci.txt
│   ├── requirements.txt
│   ├── test_newman
│   └── tests
├── docker-compose.build.yaml
├── docker-compose.prod.yaml
├── docker-compose.yaml
├── helm
├── LICENSE
├── nginx
│   ├── Dockerfile
│   └── nginx.conf
├── README.md
└── scripts
    ├── build_docker_compose.sh
    ├── hard_clean.sh
    └── run_docker_cpmpose.sh
```

- The ``app`` directory contains the main application code and related files.
- ``Dockerfile`` and ``Dockerfile.prod`` are Docker configuration files for building development and production images, 
  respectively.
- The ``entrypoint.sh`` and ``entrypoint.prod.sh`` scripts are used as entry points for the Docker containers.
- ``manage.py`` is the entry point script for running the Flask application.
- The ``src`` directory contains the project-specific code.
- ``requirements.txt`` and ``requirements.ci.txt`` list the Python dependencies for the project in development and CI 
  environments, respectively.
- The ``tests`` directory holds the unit tests for the application.
- The ``docker-compose.yaml`` and ``docker-compose.prod.yaml`` files are used to define the Docker Compose configurations for 
  development and production environments.
- The docker-compose.build.yaml file specifies the Docker Compose configuration to create an isolated environment ideal 
  fot testing and development.
- The nginx directory contains the Docker configuration for an Nginx server.
- The helm directory is for Helm chart files (if applicable).
- The scripts directory includes various utility scripts for building and running Docker Compose configurations.
