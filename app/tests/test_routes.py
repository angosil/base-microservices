def test_welcome(client):
    rv = client.get('/')
    assert rv.status_code == 200
    json_data = rv.get_json()
    assert json_data['status'] == 'success'
    assert json_data['message'] == 'Welcome to Base microservice v1.0'
