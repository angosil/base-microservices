import os

basedir = os.path.abspath(os.path.dirname(__file__))


class Config:
    API_VERSION = "v1.0"

    SECRET_KEY = os.getenv("APP_SECRET_KEY", "my_secret")

    LOG_TYPE = os.getenv("APP_LOG_TYPE", "stream")  # ["watched", "stream"]
    LOG_LEVEL = os.getenv("APP_LOG_LEVEL", "DEBUG")
    LOG_DIR = os.getenv("APP_LOG_DIR", "src/logs")  # for "watched"
    APP_LOG_NAME = os.getenv("APP_LOG_NAME", "app.log")  # for "watched"
    WWW_LOG_NAME = os.getenv("WWW_LOG_NAME", "www.log")  # for "watched"
