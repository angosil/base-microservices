from flask import jsonify

ROL_ADMIN = "ADMIN"


class WelcomeRoutes:
    app = None

    def __init__(self, app=None):
        self._app = app
        self.init_app(app)

        app.add_url_rule('/', view_func=self.welcome, methods=['GET'])

    def init_app(self, app):
        self.app = app

    def welcome(self):
        self.app.logger.info("Calling welcome endpoint")
        version = self.app.config['API_VERSION']
        return jsonify(status="success",
                       message=f'Welcome to Base microservice {version}')
