from flask import Flask
from flask_cors import CORS
from src.logger import LogSetup
from src.exception import register_error
from src.routes.welcome_routes import WelcomeRoutes

app = Flask(__name__)
app.config.from_object("src.config.Config")

# Creating All Objects

# Starting logger
logs = LogSetup()
logs.init_app(app)

# Starting app
app.logger.debug("Load cors ...")
CORS(app)
try:
    app.logger.debug("Register errors ...")
    register_error(app)
    app.logger.debug("Loading routes ...")
    WelcomeRoutes(app)
except Exception as error:
    app.logger.exception(error)
    raise error

app.logger.debug("Waiting for calls...")
