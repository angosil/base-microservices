import subprocess
from datetime import datetime


def generate():
    # Get the current version from the latest Git tag
    try:
        tag = subprocess.check_output(['git', 'describe', '--abbrev=0', '--tags']).strip().decode()
        major, minor, patch = tag.split('.')
        tag = f"{major}.{get_month(minor)}.{get_patch(minor, patch)}"
    except subprocess.CalledProcessError:
        # No tags found, start with initial version
        now = datetime.now()
        year = now.strftime("%y")
        month = now.strftime("%m")
        tag = f"{year}.{get_month(month)}.0"

    return tag


def get_month(month: str) -> str:
    if month <= '04' or month > '10':
        return '04'
    else:
        return '10'


def get_patch(month: str, patch: str) -> int:
    if month <= '04' or month > '10':
        ned_restart = '04' != month
    else:
        ned_restart = '10' != month

    if ned_restart:
        return 0
    return int(patch) + 1


if __name__ == '__main__':
    print(generate())
